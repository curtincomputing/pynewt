import math


class Ball():
    def __init__(self, parent, radius: int = 0.1, mass: float = 1,
                 len: int = 1, theta: float = -math.pi/2):
        self.radius = radius
        self.mass = mass
        self.theta = theta
        self.ddtheta = 0
        self.dtheta = 0
        self.len = len
        self.parent = parent

    def update(self, t: float):
        self.ddtheta = -self.parent.g/self.len * \
            math.cos(self.theta)  # this gives rads/s^2
        self.dtheta += self.ddtheta * t
        self.theta += self.dtheta * t
