from pygame import gfxdraw
import pygame
from numpy import *
import math
from Ball import Ball
from Constants import WHITE


class Cradle():

    def __init__(self, nballs=1, length=80, angles=[0]):
        self.angle = angle
        self.size = length
        self.g = 9.8
        self.stringLen = 1
        self.balls = []
        for i in range(nballs):
            self.balls += [Ball(self, len=length)]
        for i in range(len(angles)):
            self.balls[i].theta = angles[i]

    def render(self, screen):
        RRadius = 20
        RString = 100
        off = (30, 30)
        for i in self.balls:
            off = add(off, (RRadius*2, 0))
            x = round(math.cos(i.theta-math.pi/2*0)*RString)
            y = round(math.sin(i.theta-math.pi/2*0)*RString)
            gfxdraw.line(screen, off[0], off[1], off[0]-x, off[1]-y, WHITE)
            gfxdraw.filled_circle(
                screen, off[0] - x, off[1] - y, RRadius, WHITE)
            gfxdraw.aacircle(
                screen, off[0] - x, off[1] - y, RRadius, WHITE)

    def update(self, t):
          # milliseconds to seconds
        for i in range(len(self.balls)):
            curr = self.balls[i]

            curr.update(t)

            print("{}".format(i))
            # if(i==0):
            #    print("{0}".format(curr.dtheta),end="\r")

            self.checkCollisions(i)

    def checkCollisions(self, i):
        curr = self.balls[i]
        currpos = (curr.len*math.cos(curr.theta),
                   curr.len*math.sin(curr.theta))
        w = curr.dtheta
        # for the velocity returns 1 if negative -1 if positive
        di = int(w < 0)*1 + int(w > 0)*-1
        if (i+di > -1 and i+di < len(self.balls) and not di == 0):
            # this collision detection works for two balls with different radii
            # the collision occurs if the balls are touhcing in any way.
            tmp = self.balls[i+di]  # gets either the next or previous
            offset = (curr.radius + tmp.radius)*1
            tmppos = (tmp.len*math.cos(tmp.theta) - offset*di,
                      tmp.len*math.sin(tmp.theta))
            x, y = subtract(currpos, tmppos)
            distance = math.hypot(x, y)
            print("distance {0} offset {1} i {2}:{3} {4}".format(distance,
                                                                 offset, i, di, w))
            if(distance <= offset):  # collision
                tmp.dtheta += math.cos(curr.ddtheta)*curr.dtheta#the x component
                curr.dtheta = 0
