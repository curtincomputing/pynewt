import pygame
import math
from pygame import gfxdraw
from Constants import *


class Renderer():
    def __init__(self, size: tuple = (600, 300)):
        self.screen = pygame.display.set_mode(size)

        self.clock = pygame.time.Clock()
        self.exit = False
        self.objects = []

    def addObj(self, objArr):
        self.objects.append(objArr)

    def run(self):
        try:
            while(not self.exit):

                for event in pygame.event.get():
                    if(event.type == pygame.QUIT):
                        self.exit = True
                self.render()
                self.update()
                pygame.display.flip()
        finally:
            pygame.quit()

    def render(self):
        self.screen.fill(BLACK)
        for i in self.objects:
            i.render(self.screen)

    def update(self):
        t = self.clock.tick()/1000  # milliseconds to seconds
        for i in self.objects:
            i.update(t)
