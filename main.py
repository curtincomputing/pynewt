from Cradle import Cradle
from Ball import Ball
import math
from Render import Renderer

ang = [-math.pi/6] * 2
ang += [3*math.pi/2] * 3

c = Cradle(nballs=5, length=1, angles=ang)
r = Renderer()
r.addObj(c)
r.run()
